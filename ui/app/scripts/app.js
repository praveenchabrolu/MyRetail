'use strict';

/**
 * @ngdoc overview
 * @name retailApp
 * @description
 * # retailApp
 *
 * Main module of the application.
 */
angular
  .module('retailApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
  // angular.module('retailApp').config(['$qProvider', function ($qProvider) {
  //    $qProvider.errorOnUnhandledRejections(false);
  // }]);
