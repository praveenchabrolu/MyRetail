'use strict';

/**
 * @ngdoc function
 * @name retailApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the retailApp
 */
angular.module('retailApp')
.controller('MainCtrl',['$scope', '$http', function ($scope, $http) {
  $scope.awesomeThings = [
    'HTML5 Boilerplate',
    'AngularJS',
    'Karma'
  ];
  $scope.item = {};
  $scope.id="13860428"
  var URL = "http://localhost:8080/product";
  $scope.error = null;
  var method = 'get';

  $scope.getProduct = function(id){
    var url = URL+"/"+id;
    $http({method: method, url: url}).then(function(response){
      $scope.item = response.data;
      $scope.error = null;
      $scope.success = null;
    },function(response){
      console.log(response);
      $scope.error = "Sorry Product not found for id :"+id;
      $scope.success = null;
      $scope.item = null;
    });
  }


  $scope.savePrice = function(product,id){
    var url = URL+"/"+id;
    $http.put(url,product.current_price).then(function(response){
      $scope.success = "Price Saved Successfully";
      $scope.error = null;
    },function(response){
      $scope.success = null;
      $scope.error = "Sorry Price Information cannot be saved at this time";
    })
  }

  $scope.getProduct($scope.id);
}]);
