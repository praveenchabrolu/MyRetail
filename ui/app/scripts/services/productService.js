'use strict';
/**
* @ngdoc function
* @name retailApp.service:Product
* @description
* # Product service
* Service of the retailApp
**/
angular.module('retailApp')
.Service('Product', ['$http', function ($http) {
  this.URL = '/product';

  this.getAll = function(){
    return $http.get(this.URL);
  }
  this.get = function(id){
    var getUrl = this.URL + '/' + id;
    return $http.get(getUrl);
  };

  this.update = function(product){
    return $http.put(product,url);
  };
}]);
