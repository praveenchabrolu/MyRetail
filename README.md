Steps to run the maven application:

1. Install Java, Maven, Postman or equivalent tool in your local
2. Install MongoDB in your local
   Create a myRetail Database in the local mongo DB setup
   Create a price collection in the myRetail Connection
   Insert test documents into the price collection with the id, value, currencyCode parameters
3. Clone or download the project from https://praveenchabrolu@gitlab.com/praveenchabrolu/MyRetail.git
4. Clean and build the application 
5. Import the project as maven project to eclipse 
6. Open the App.java under myReatil\src\main\java\com\praveen\myRetail\App.java
7. Run the App.java as spring boot application
8. After the server starts up, Go to postman
9. Use the link http://localhost:8080/product/{id} in Postman, select the GET as the request type and execute the request
10. You will see the result in the response body.

Steps to run the UI Client:

1. Install node, git
2. Install bower, grunt using npm cmd:  "npm install -g grunt-cli bower"
3. Go to ui folder in your workspace, and run "npm install" and "bower install"
4. After bower and node_modules are successfully installed run "grunt serve"
5. This will automatically load the application in a web browser or else the application can be loaded using the url -> "http://localhost:9003/#!/"
6. Screen will contain input filed to accept the "id" of the product and load the product depends on id when you click "Get Product" button
7. Below container will display the production information and also allows the user to edit the price and currency code values
8. Edit the values in the "Price" and "Currency Code" fields, and click on "Update" button, this will save the values entered


About the application:

The Project is implemented as a spring boot application. 
Used Maven project setup  
Used MongoDB as the Database 
The REST API will call the http://redsky.target.com with id parameter to get the Product information and also gets the price information from the local MongoDB and combines the price information and production information and provides the response object

Get API URL : http://localhost:8080/product/{id}
 Sample JSON result: 
 {
  "id": "13860428",
  "name": "The Big Lebowski (Blu-ray)",
  "current_price": {
    "id": "13860428",
    "value": "14.9",
    "currency_code": "USD"
  }
 }
 
PUT API URL: http://localhost:8080/product/{id}
  Request JSON: 
  {
    "id": "13860428",
    "value": "14.9",
    "currency_code": "USD"
  }
  