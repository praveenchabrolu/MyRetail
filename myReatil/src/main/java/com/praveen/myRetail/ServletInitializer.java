package com.praveen.myRetail;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {
	protected SpringApplicationBuilder config(SpringApplicationBuilder application) {
		return application.sources(App.class);
	}
}
