package com.praveen.myRetail.services;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.WebRequest;

import com.praveen.myRetail.Exception.NoContentFoundException;
import com.praveen.myRetail.Exception.RestErrorInfo;
import com.praveen.myRetail.domain.Price;
import com.praveen.myRetail.domain.Product;
import com.praveen.myRetail.repository.PriceRepository;
/**
 * @author Praveen
 * 
 * ProductController.java
 * Handles all the methods related to product information
 */
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "app")
@RestController
@RequestMapping("/product")
public class ProductController implements ApplicationEventPublisherAware {
	private static final Logger log = (Logger) LoggerFactory.getLogger(ProductController.class);
	protected ApplicationEventPublisher eventPublisher;
	private PriceRepository repository;
	private String redskyUrl;
	
	/**
	 * Constructor for ProductController
	 */
	@Autowired
	public ProductController(PriceRepository repository) {
		this.repository = repository;
	}

	/**
	 * 
	 * @param id
	 * @return Product
	 * 
	 *  Gets the Product information by id from redsky 
	 */
	private com.target.redsky.Product getProduct(String id) {
		RestTemplate restTemplate = new RestTemplate();
		String url = redskyUrl.replace("{id}", id);
		com.target.redsky.Product product = restTemplate.getForObject(url, com.target.redsky.Product.class);
		log.info(product.toString());
		return product;
	}

	/**
	 * 
	 * @param id : id of the Product
	 * @return Product : returns the Product object by id
	 * @throws Exception
	 * 
	 * Get the Product price information by id from local MongoDB  
	 */
	@CrossOrigin
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = { "application/json", "application/xml" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody Product getProductPrice(@PathVariable String id) throws Exception {
		Product product = new Product();
		com.target.redsky.Product product1 = getProduct(id);
		if (product1 == null) {
			throw new NoContentFoundException("No Product found");
		}
		product.setId(id);
		product.setName(product1.getProduct().getItem().getProductDescription().getTitle());
		product.setPrice(repository.findById(id));
		return product;

	}
	
	/**
	 * 
	 * @param id : id of the Product
	 * @param product : expects Product object
	 * @return boolean : returns if the save is success or not
	 * @throws Exception
	 * 
	 * Update the Product price information
	 */
	@CrossOrigin
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = { "application/json", "application/xml" })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody boolean updateProductPrice(@PathVariable String id, @RequestBody Price price) throws Exception {
		if(price == null){
			throw new NoContentFoundException("No Price found");
		}
		boolean updated = repository.update(price);
		return updated;
	}

	public void setApplicationEventPublisher(ApplicationEventPublisher eventPublisher) {
		this.eventPublisher = eventPublisher;
	}

	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ExceptionHandler(NoContentFoundException.class)
	public @ResponseBody RestErrorInfo handleNoContentFoundException(NoContentFoundException nce, WebRequest request,
			HttpServletResponse response) {
		log.info("NoContentFoundExcption handler: " + nce.getMessage());
		return new RestErrorInfo(nce, "Sorry I couldn't find data");
	}

	public String getRedskyUrl() {
		return redskyUrl;
	}

	public void setRedskyUrl(String redskyUrl) {
		this.redskyUrl = redskyUrl;
	}
}
