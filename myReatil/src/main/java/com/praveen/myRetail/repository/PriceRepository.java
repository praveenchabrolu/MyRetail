package com.praveen.myRetail.repository;

import java.io.IOException;
import java.util.Map;

import org.bson.Document;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;
import com.praveen.myRetail.domain.Price;

/**
 * 
 * @author Praveen
 *
 *         PriceRepository
 * 
 */

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "app")
public class PriceRepository {

	private String url;
	private String db;
	private String PRICE_COLLECTION = "price";

	private MongoClient mongoClient;
	private MongoCollection<Document> priceCollection;

	public PriceRepository() {
	}

	private void initalize() {
		if (mongoClient == null || priceCollection == null) {
			MongoClientURI uri = new MongoClientURI(url);
			mongoClient = new MongoClient(uri);
			MongoDatabase database = mongoClient.getDatabase(db);
			priceCollection = database.getCollection(PRICE_COLLECTION);
		}
	}

	@Override
	public void finalize() {
		mongoClient.close();
	}

	@SuppressWarnings("unchecked")
	private Document getDocument(Object obj) {
		BasicDBObject objectBson = new BasicDBObject();
		ObjectMapper objMapper = new ObjectMapper();
		Document doc = new Document();
		if (obj != null) {
			Map<String, Object> map = objMapper.convertValue(obj, Map.class);
			objectBson.putAll(map);
			doc = Document.parse(objectBson.toString());
		}
		return doc;
	}

	/**
	 * 
	 * @param id
	 *            : id of the product
	 * @return Price : returns price document
	 * @throws JsonParseException
	 *             : throws JSONParseException
	 * @throws JsonMappingException
	 *             : throws JSONMappingException
	 * @throws IOException
	 *             : throws IOException
	 * 
	 *             Get the price document of a product by product id
	 */
	public Price findById(String id) throws JsonParseException, JsonMappingException, IOException {
		initalize();
		BasicDBObject fields = new BasicDBObject();
		fields.put("id", id);
		MongoCursor<Document> cursor = priceCollection.find(fields).iterator();
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Price price = mapper.readValue(cursor.next().toJson(), Price.class);
		cursor.close();
		return price;
	}

	/**
	 * 
	 * @param price
	 *            : Price object input
	 * @return boolean : returns boolean if the document update is success or
	 *         not
	 * @throws JsonParseException
	 *             : throws JsonParseException
	 * @throws JsonMappingException
	 *             : throws JsonMappingException
	 * @throws IOException
	 *             : throws IOException
	 * 
	 *             Update the price document by product id
	 */
	public boolean update(Price price) throws JsonParseException, JsonMappingException, IOException {
		initalize();
		BasicDBObject fields = new BasicDBObject();
		fields.put("id", price.getId());
		Document priceDoc = getDocument(price);
		UpdateResult updatedResult = priceCollection.replaceOne(fields, priceDoc);
		boolean updated = updatedResult.isModifiedCountAvailable();
		return updated;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDb() {
		return db;
	}

	public void setDb(String db) {
		this.db = db;
	}

}
