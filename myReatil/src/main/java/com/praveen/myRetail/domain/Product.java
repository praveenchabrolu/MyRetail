package com.praveen.myRetail.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

//: {"id":13860428,"name":"The Big Lebowski (Blu-ray) (Widescreen)","current_price":{"value": 13.49,"currency_code":"USD"}}
public class Product implements Serializable {
	private static final long serialVersionUID = -6197863979071213445L;
	private String id;
	private String name;
	@JsonProperty("current_price")
	private Price price;

	public Product() {
		super();
	}

	public Product(String id, String name, Price price) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

}
