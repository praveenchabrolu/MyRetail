package com.praveen.myRetail.domain;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@Document(collection="price")
@JsonIgnoreProperties
public class Price implements Serializable {

	private static final long serialVersionUID = 7796407771452743138L;
	@Id
	private String id;
	private String value;
	@JsonProperty("currency_code")
	private String currencyCode;

	public Price(){
		super();
	}
	
	public Price(String id, String value, String currencyCode) {
		super();
		this.id = id;
		this.value = value;
		this.currencyCode = currencyCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	@Override
	public String toString() {
		return "Price{" + "Value='" + value + '\'' + ", currency_code=" + currencyCode + '}';
	}

}
