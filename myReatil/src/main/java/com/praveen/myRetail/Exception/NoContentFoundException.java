package com.praveen.myRetail.Exception;

public class NoContentFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public NoContentFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoContentFoundException() {
		super();
	}

	public NoContentFoundException(String message) {
		super(message);
	}

	public NoContentFoundException(Throwable cause) {
		super(cause);
	}

}
