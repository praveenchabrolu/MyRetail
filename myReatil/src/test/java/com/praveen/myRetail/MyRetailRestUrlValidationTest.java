package com.praveen.myRetail;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.praveen.myRetail.App;
import com.praveen.myRetail.domain.Price;
import com.praveen.myRetail.domain.Product;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class MyRetailRestUrlValidationTest {
	public static final String REST_SERVICE_URI = "http://localhost:8080";
	@Autowired
	private MockMvc mockMvc;
	@LocalServerPort
	private int port;

	@Test
	public void shouldReturnDefaultMessage() throws Exception {
		// Convert object to JSON string and pretty print
		MockHttpServletResponse response = this.mockMvc.perform(get("http://localhost:" + port + "product/13860428"))
				.andDo(print()).andExpect(status().isOk()).andReturn().getResponse();
		assertNotNull(response.getContentAsString());
	}

	@Test
	public void productUpdateTest() throws Exception {
		Price price = new Price("13860428", "188.99", "USD");
		
		ObjectMapper mapper = new ObjectMapper();
		String originalJsonString = mapper.writeValueAsString(price);
		
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.put(REST_SERVICE_URI + "/product/13860428", price);
		Product product = restTemplate.getForObject(REST_SERVICE_URI + "/product/13860428", Product.class);
		
		String expectedJsonString = mapper.writeValueAsString(product.getPrice());
		assertEquals(originalJsonString, expectedJsonString);
	}
}