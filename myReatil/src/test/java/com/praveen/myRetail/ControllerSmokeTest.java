package com.praveen.myRetail;
/**
 * The is a sanity testing for the ProductController
 * 
 * @author Praveen
 * 
 */
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.praveen.myRetail.App;
import com.praveen.myRetail.services.ProductController;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=App.class)
public class ControllerSmokeTest {
	@Autowired
	private ProductController productController;

	@Test
	public void contexLoads() throws Exception {
		assertThat(productController).isNotNull();
	}
}
